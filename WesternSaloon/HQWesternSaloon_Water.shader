// Shader created with Shader Forge v1.18 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.18;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:0,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:False,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:0,x:34430,y:32114,varname:node_0,prsc:2|diff-5393-OUT,spec-7707-OUT,gloss-8349-OUT,normal-215-OUT,transm-29-OUT,lwrap-29-OUT,alpha-6219-OUT,refract-5700-OUT;n:type:ShaderForge.SFN_Slider,id:13,x:33424,y:32440,ptovrint:False,ptlb:Refraction Intensity,ptin:_RefractionIntensity,varname:_RefractionIntensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1,max:0.3;n:type:ShaderForge.SFN_Multiply,id:14,x:34074,y:32506,varname:node_14,prsc:2|A-16-OUT,B-220-OUT;n:type:ShaderForge.SFN_ComponentMask,id:16,x:33872,y:32448,varname:node_16,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-25-RGB;n:type:ShaderForge.SFN_Tex2d,id:25,x:33622,y:32086,ptovrint:False,ptlb:Normal Map (Refraction),ptin:_NormalMapRefraction,varname:_Refraction,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:23b88f1a16914624aa4967047cc7fdbe,ntxv:2,isnm:False|UVIN-749-UVOUT;n:type:ShaderForge.SFN_Vector1,id:29,x:34174,y:32308,varname:node_29,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:215,x:33945,y:32291,varname:node_215,prsc:2|A-216-OUT,B-25-RGB,T-13-OUT;n:type:ShaderForge.SFN_Vector3,id:216,x:33844,y:32168,varname:node_216,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_Multiply,id:220,x:33904,y:32623,varname:node_220,prsc:2|A-13-OUT,B-221-OUT;n:type:ShaderForge.SFN_Vector1,id:221,x:33732,y:32643,varname:node_221,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Tex2d,id:61,x:33448,y:31400,ptovrint:False,ptlb:DirtMask,ptin:_DirtMask,varname:node_61,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a4f8a23f87b5305409d41fbc8017f7ec,ntxv:0,isnm:False|UVIN-591-UVOUT;n:type:ShaderForge.SFN_Vector1,id:3503,x:32216,y:33793,varname:node_3503,prsc:2,v1:16;n:type:ShaderForge.SFN_Add,id:8351,x:33690,y:31720,varname:node_8351,prsc:2|A-61-A,B-7542-A;n:type:ShaderForge.SFN_Color,id:3226,x:34144,y:31653,ptovrint:False,ptlb:DirtColor,ptin:_DirtColor,varname:node_3226,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.397065,c2:0.3331571,c3:0.1956156,c4:1;n:type:ShaderForge.SFN_Lerp,id:5393,x:34336,y:31526,varname:node_5393,prsc:2|A-7542-RGB,B-3226-RGB,T-61-A;n:type:ShaderForge.SFN_Color,id:7542,x:34003,y:31381,ptovrint:False,ptlb:Water Color,ptin:_WaterColor,varname:node_7542,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5147171,c2:0.4671224,c3:0.08327018,c4:0.1294118;n:type:ShaderForge.SFN_Slider,id:7707,x:34079,y:31837,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_7707,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.27,max:1;n:type:ShaderForge.SFN_Slider,id:8349,x:34079,y:31957,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_8349,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.948,max:1;n:type:ShaderForge.SFN_Time,id:8433,x:32992,y:32141,varname:node_8433,prsc:2;n:type:ShaderForge.SFN_Panner,id:749,x:33423,y:32066,varname:node_749,prsc:2,spu:1,spv:1|UVIN-1755-UVOUT,DIST-7964-OUT;n:type:ShaderForge.SFN_TexCoord,id:1755,x:33285,y:31900,varname:node_1755,prsc:2,uv:0;n:type:ShaderForge.SFN_Divide,id:7964,x:33286,y:32208,varname:node_7964,prsc:2|A-8433-T,B-181-OUT;n:type:ShaderForge.SFN_RemapRange,id:181,x:33100,y:32312,varname:node_181,prsc:2,frmn:0,frmx:1,tomn:1,tomx:200|IN-2910-OUT;n:type:ShaderForge.SFN_Slider,id:2910,x:32692,y:32364,ptovrint:False,ptlb:Speed Divide,ptin:_SpeedDivide,varname:node_2910,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_DepthBlend,id:3489,x:33703,y:31926,varname:node_3489,prsc:2|DIST-6918-OUT;n:type:ShaderForge.SFN_Multiply,id:6219,x:33861,y:31837,varname:node_6219,prsc:2|A-8351-OUT,B-3489-OUT;n:type:ShaderForge.SFN_Slider,id:7274,x:33321,y:31813,ptovrint:False,ptlb:Depth Blend,ptin:_DepthBlend,varname:node_7274,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.075,max:1;n:type:ShaderForge.SFN_Multiply,id:5700,x:34236,y:32406,varname:node_5700,prsc:2|A-14-OUT,B-3489-OUT;n:type:ShaderForge.SFN_RemapRange,id:6918,x:33551,y:31907,varname:node_6918,prsc:2,frmn:0,frmx:1,tomn:0,tomx:0.4|IN-7274-OUT;n:type:ShaderForge.SFN_Panner,id:591,x:33231,y:31400,varname:node_591,prsc:2,spu:1,spv:1|UVIN-6379-UVOUT,DIST-8771-OUT;n:type:ShaderForge.SFN_TexCoord,id:6379,x:32793,y:31211,varname:node_6379,prsc:2,uv:0;n:type:ShaderForge.SFN_Divide,id:8771,x:33011,y:31426,varname:node_8771,prsc:2|A-1009-T,B-8864-OUT;n:type:ShaderForge.SFN_Time,id:1009,x:32782,y:31456,varname:node_1009,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:8864,x:33030,y:31598,varname:node_8864,prsc:2,frmn:-1,frmx:1,tomn:-600,tomx:600|IN-7897-OUT;n:type:ShaderForge.SFN_Slider,id:7897,x:32679,y:31706,ptovrint:False,ptlb:Dirt Direction,ptin:_DirtDirection,varname:node_7897,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-1,max:1;n:type:ShaderForge.SFN_Vector1,id:9113,x:32408,y:33985,varname:node_9113,prsc:2,v1:16;n:type:ShaderForge.SFN_Vector1,id:224,x:32728,y:34305,varname:node_224,prsc:2,v1:16;n:type:ShaderForge.SFN_Vector1,id:286,x:32792,y:34369,varname:node_286,prsc:2,v1:16;n:type:ShaderForge.SFN_Vector1,id:1359,x:32856,y:34433,varname:node_1359,prsc:2,v1:16;n:type:ShaderForge.SFN_Vector1,id:4996,x:32920,y:34497,varname:node_4996,prsc:2,v1:16;proporder:61-25-7542-3226-7707-8349-13-7274-2910-7897;pass:END;sub:END;*/

Shader "HQWesternSaloon/HQWesternSaloon_Water" {
    Properties {
        _DirtMask ("DirtMask", 2D) = "white" {}
        _NormalMapRefraction ("Normal Map (Refraction)", 2D) = "black" {}
        _WaterColor ("Water Color", Color) = (0.5147171,0.4671224,0.08327018,0.1294118)
        _DirtColor ("DirtColor", Color) = (0.397065,0.3331571,0.1956156,1)
        _Specular ("Specular", Range(0, 1)) = 0.27
        _Gloss ("Gloss", Range(0, 1)) = 0.948
        _RefractionIntensity ("Refraction Intensity", Range(0, 0.3)) = 0.1
        _DepthBlend ("Depth Blend", Range(0, 1)) = 0.075
        _SpeedDivide ("Speed Divide", Range(0, 1)) = 1
        _DirtDirection ("Dirt Direction", Range(-1, 1)) = -1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float _RefractionIntensity;
            uniform sampler2D _NormalMapRefraction; uniform float4 _NormalMapRefraction_ST;
            uniform sampler2D _DirtMask; uniform float4 _DirtMask_ST;
            uniform float4 _DirtColor;
            uniform float4 _WaterColor;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _SpeedDivide;
            uniform float _DepthBlend;
            uniform float _DirtDirection;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                half3 normalDir : NORMAL;
                half3 tangentDir : TEXCOORD2;
                half3 bitangentDir : TEXCOORD3;
                float4 screenPos : TEXCOORD4;
                float4 projPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD9;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                half3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_8433 = _Time + _TimeEditor;
                float2 node_749 = (i.uv0+(node_8433.g/(_SpeedDivide*199.0+1.0))*float2(1,1));
                half4 _NormalMapRefraction_var = tex2D(_NormalMapRefraction,TRANSFORM_TEX(node_749, _NormalMapRefraction));
                float node_3489 = saturate((sceneZ-partZ)/(_DepthBlend*0.4+0.0));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((_NormalMapRefraction_var.rg*(_RefractionIntensity*0.2))*node_3489);
                half4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                half3x3 tangentTransform = half3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                half3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                half3 normalLocal = lerp(half3(0,0,1),_NormalMapRefraction_var.rgb,_RefractionIntensity);
                half3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                half3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                half3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                half3 lightColor = _LightColor0.rgb;
                half3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                half3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                d.boxMax[0] = unity_SpecCube0_BoxMax;
                d.boxMin[0] = unity_SpecCube0_BoxMin;
                d.probePosition[0] = unity_SpecCube0_ProbePosition;
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.boxMax[1] = unity_SpecCube1_BoxMax;
                d.boxMin[1] = unity_SpecCube1_BoxMin;
                d.probePosition[1] = unity_SpecCube1_ProbePosition;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                UnityGI gi = UnityGlobalIllumination (d, 1, gloss, normalDirection);
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                half NdotL = max(0, dot( normalDirection, lightDirection ));
                half LdotH = max(0.0,dot(lightDirection, halfDirection));
                half specularMonochrome = _Specular;
                half NdotV = max(0.0,dot( normalDirection, viewDirection ));
                half NdotH = max(0.0,dot( normalDirection, halfDirection ));
                half VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
                half3 specularColor = float3(_Specular,_Specular,_Specular);
                half3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                half3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                half3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                half node_29 = 1.0;
                half3 w = half3(node_29,node_29,node_29)*0.5; // Light wrapping
                half3 NdotLWrap = NdotL * ( 1.0 - w );
                half3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                half3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_29,node_29,node_29);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                NdotLWrap = max(float3(0,0,0), NdotLWrap);
                half3 directDiffuse = ((forwardLight+backLight) + ((1 +(fd90 - 1)*pow((1.00001-NdotLWrap), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL))*(0.5-max(w.r,max(w.g,w.b))*0.5) * attenColor;
                half3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float4 node_1009 = _Time + _TimeEditor;
                float2 node_591 = (i.uv0+(node_1009.g/(_DirtDirection*600.0+0.0))*float2(1,1));
                float4 _DirtMask_var = tex2D(_DirtMask,TRANSFORM_TEX(node_591, _DirtMask));
                half3 diffuseColor = lerp(_WaterColor.rgb,_DirtColor.rgb,_DirtMask_var.a);
                diffuseColor *= 1-specularMonochrome;
                half3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                half3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,((_DirtMask_var.a+_WaterColor.a)*node_3489)),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float _RefractionIntensity;
            uniform sampler2D _NormalMapRefraction; uniform float4 _NormalMapRefraction_ST;
            uniform sampler2D _DirtMask; uniform float4 _DirtMask_ST;
            uniform float4 _DirtColor;
            uniform float4 _WaterColor;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _SpeedDivide;
            uniform float _DepthBlend;
            uniform float _DirtDirection;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                half3 normalDir : NORMAL;
                half3 tangentDir : TEXCOORD2;
                half3 bitangentDir : TEXCOORD3;
                float4 screenPos : TEXCOORD4;
                float4 projPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                half3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_8433 = _Time + _TimeEditor;
                float2 node_749 = (i.uv0+(node_8433.g/(_SpeedDivide*199.0+1.0))*float2(1,1));
                half4 _NormalMapRefraction_var = tex2D(_NormalMapRefraction,TRANSFORM_TEX(node_749, _NormalMapRefraction));
                float node_3489 = saturate((sceneZ-partZ)/(_DepthBlend*0.4+0.0));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((_NormalMapRefraction_var.rg*(_RefractionIntensity*0.2))*node_3489);
                half4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                half3x3 tangentTransform = half3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                half3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                half3 normalLocal = lerp(half3(0,0,1),_NormalMapRefraction_var.rgb,_RefractionIntensity);
                half3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                half3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                half3 lightColor = _LightColor0.rgb;
                half3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                half NdotL = max(0, dot( normalDirection, lightDirection ));
                half LdotH = max(0.0,dot(lightDirection, halfDirection));
                half specularMonochrome = _Specular;
                half NdotV = max(0.0,dot( normalDirection, viewDirection ));
                half NdotH = max(0.0,dot( normalDirection, halfDirection ));
                half VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
                half3 specularColor = float3(_Specular,_Specular,_Specular);
                half3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                half3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                half node_29 = 1.0;
                half3 w = half3(node_29,node_29,node_29)*0.5; // Light wrapping
                half3 NdotLWrap = NdotL * ( 1.0 - w );
                half3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                half3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_29,node_29,node_29);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                NdotLWrap = max(float3(0,0,0), NdotLWrap);
                half3 directDiffuse = ((forwardLight+backLight) + ((1 +(fd90 - 1)*pow((1.00001-NdotLWrap), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL))*(0.5-max(w.r,max(w.g,w.b))*0.5) * attenColor;
                float4 node_1009 = _Time + _TimeEditor;
                float2 node_591 = (i.uv0+(node_1009.g/(_DirtDirection*600.0+0.0))*float2(1,1));
                float4 _DirtMask_var = tex2D(_DirtMask,TRANSFORM_TEX(node_591, _DirtMask));
                half3 diffuseColor = lerp(_WaterColor.rgb,_DirtColor.rgb,_DirtMask_var.a);
                diffuseColor *= 1-specularMonochrome;
                half3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                half3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * ((_DirtMask_var.a+_WaterColor.a)*node_3489),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _DirtMask; uniform float4 _DirtMask_ST;
            uniform float4 _DirtColor;
            uniform float4 _WaterColor;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _DirtDirection;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
/////// Vectors:
                half3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 node_1009 = _Time + _TimeEditor;
                float2 node_591 = (i.uv0+(node_1009.g/(_DirtDirection*600.0+0.0))*float2(1,1));
                float4 _DirtMask_var = tex2D(_DirtMask,TRANSFORM_TEX(node_591, _DirtMask));
                half3 diffColor = lerp(_WaterColor.rgb,_DirtColor.rgb,_DirtMask_var.a);
                half specularMonochrome = _Specular;
                diffColor *= (1.0-specularMonochrome);
                float roughness = 1.0 - _Gloss;
                float3 specColor = float3(_Specular,_Specular,_Specular);
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
